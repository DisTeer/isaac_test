using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class VolumeUp : MonoBehaviour
{
    public Sprite[] volume;
    public int currentIndex = 0;

    void Update()
    {
        if (Input.GetKeyDown("right"))
        {
            for (int i = 0; i < volume.Length; i++)
            {
                if (gameObject.GetComponent<Image>().sprite == volume[i] && currentIndex == i)
                {
                    StartCoroutine(WaitPlus(i));
                }
            }
        }
        if (Input.GetKeyDown("left"))
        {
            for (int i = 0; i < volume.Length; i++)
            {
                if (gameObject.GetComponent<Image>().sprite == volume[i] && currentIndex == i)
                {
                    StartCoroutine(WaitMinus(i));
                }
            }          
        }
    }
    public IEnumerator WaitPlus(int i)
    {
        yield return new WaitForSeconds(0.05f);
        currentIndex = i + 1;
        gameObject.GetComponent<Image>().sprite = volume[currentIndex];
    }
    public IEnumerator WaitMinus(int i)
    {
        yield return new WaitForSeconds(0.05f);
        currentIndex = i - 1;
        gameObject.GetComponent<Image>().sprite = volume[currentIndex];
    }

}
