using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchArrow : MonoBehaviour
{
    public GameObject[] placements;
    public GameObject arrow;
    public int currentIndex = 0;
    void Update()
    {
        if (Input.GetKeyDown("down"))
        {
            for(int i = 0; i < placements.Length; i++)
            {
                if (arrow.transform.position == placements[i].transform.position && currentIndex == i)
                {
                    StartCoroutine(WaitPlus(i));
                }
            }
        }
        if (Input.GetKeyDown("up"))
        {
            for (int i = 0; i < placements.Length; i++)
            {
                if (arrow.transform.position == placements[i].transform.position && currentIndex == i)
                {
                    StartCoroutine(WaitMinus(i));
                }  
            }
        }
    }
    public IEnumerator WaitPlus(int i)
    {
        yield return new WaitForSeconds(0.1f);
        currentIndex = i + 1;
        arrow.transform.position = placements[currentIndex].transform.position;
    }
    public IEnumerator WaitMinus(int i)
    {
        yield return new WaitForSeconds(0.1f);
        currentIndex = i - 1;
        arrow.transform.position = placements[currentIndex].transform.position;
    }
}
