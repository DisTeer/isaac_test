using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuSwitch : MonoBehaviour
{
    public GameObject actual;
    public GameObject goal;
    public GameObject back;
    public GameObject characterSelect;
    public GameObject optionsMenu;




    private void Update()
    {
        if (Input.GetKeyDown("return"))
        {
            actual.SetActive(false);
            if (actual == goal)
            {
                if (actual.GetComponentInChildren<SwitchArrow>().currentIndex == 0)
                {
                    characterSelect.SetActive(true);
                    SceneManager.LoadScene("Game");
                }
                else if (actual.GetComponentInChildren<SwitchArrow>().currentIndex == 4)
                {
                    optionsMenu.SetActive(true);
                }
            }
            else
            {
                goal.SetActive(true);
            }
        }
        if (Input.GetKeyDown("escape"))
        {
            actual.SetActive(false);
            back.SetActive(true);
            if (actual == back)
            {
                Application.Quit();
            }
        }
    }
}
