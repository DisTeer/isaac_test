using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedFiles : MonoBehaviour
{
    public GameObject[] movements;
    public GameObject files;
    public Animator select;
    public Animator filesR;
    public Animator stopAnim;
    public Animator filesRplus;
    public int currentIndex = 0;

    void Update()
    {
        if (Input.GetKeyDown("right"))
        {
            select.SetBool("selected", true);
           
            for (int i = 0; i < movements.Length; i++)
            {
                if (files.transform.position == movements[i].transform.position && currentIndex == i)
                {
                    StartCoroutine(WaitPlus(i));
                }
            }
            filesR.SetBool("files_mov", true);
            filesRplus.SetBool("choose", true);
        }
        if (Input.GetKeyDown("left"))
        {
            select.SetBool("selected", false);
           
            for (int i = 0; i < movements.Length; i++)
            {
                if (files.transform.position == movements[i].transform.position && currentIndex == i)
                {

                    StartCoroutine(WaitMinus(i));
                }
                filesR.SetBool("files_mov", false);
                filesRplus.SetBool("choose", false);
            }
        }
    }
    public IEnumerator WaitPlus(int i)
    {
        yield return new WaitForSeconds(0.2f);
        currentIndex = i + 1;
        files.transform.position = movements[currentIndex].transform.position;
    }
    public IEnumerator WaitMinus(int i)
    {
        yield return new WaitForSeconds(0.2f);
        currentIndex = i - 1;
        files.transform.position = movements[currentIndex].transform.position;
    }
}
