using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InventoryGestion : MonoBehaviour
{
    private int coinInventory = 0;
    private int keyInventory = 0;
    private int bombInventory = 0;

    public TextMeshProUGUI coinText;
    public TextMeshProUGUI keyText;
    public TextMeshProUGUI bombText;

    public static InventoryGestion instance;
    private void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }

    public void PickCoin (int coinValue)
    {
        coinInventory += coinValue;
        coinText.text = coinInventory.ToString("00");
    }
    public void PickKey(int keyValue)
    {
        keyInventory += keyValue;
        keyText.text = keyInventory.ToString("00");
    }
    public void PickBomb(int bombValue)
    {
        bombInventory += bombValue;
        bombText.text = bombInventory.ToString("00");
    }

}
