using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 2f;
    public Rigidbody2D rb;
    public Animator animatorBody;
    public Animator animatorHead;


    Vector2 movement;

    private void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        animatorBody.SetFloat("Horizontal", movement.x);
        animatorBody.SetFloat("Vertical", movement.y);
        animatorBody.SetFloat("Speed", movement.sqrMagnitude);
        animatorHead.SetFloat("Horizontal", movement.x);
        animatorHead.SetFloat("Vertical", movement.y);
        animatorHead.SetFloat("Speed", movement.sqrMagnitude);
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
}
