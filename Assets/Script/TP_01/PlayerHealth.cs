using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 6;
    public int health;
    public AdvancedHealth healthBar;
    public AudioClip isaacHit;
    public AudioClip isaacDeath;
    public AudioSource audio;
    public EventManger eventManager;

    private void Awake()
    {
        audio = gameObject.GetComponent<AudioSource>();
        health = maxHealth;
    }
    public void TakeDamage(int damage)
    {
        if (health - damage <= 0)
        {
            audio.clip = isaacDeath;
            audio.Play();
            health = 0;
            StartCoroutine(Die());
        }
        else if (health - damage > 0)
        {
            audio.clip = isaacHit;
            audio.Play();
            eventManager.OnDamage.Invoke();
            health -= damage;
        }
        healthBar.setHealth(health);
    }
    public void Heal (int heal)
    {
        if (health + heal <= maxHealth)
        {
            health += heal;
        }
       else
        {
            health = maxHealth;
        }
        healthBar.setHealth(health);
    }
    public IEnumerator Die()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
