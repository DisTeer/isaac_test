using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private String timerString;
    public Animator transition;
    float time;
    string hour;
    string minute;
    string second;

    private void Awake()
    {
        time = Time.timeSinceLevelLoad;
        hour = ((int)time / 3600).ToString("00");
        minute = ((int)time / 60).ToString("00");
        second = (time % 60).ToString("00");
    }

    void Update()
    {
        time = Time.timeSinceLevelLoad;
        hour = ((int)time / 3600).ToString("00");
        minute = ((int)time / 60).ToString("00");
        second = (time % 60).ToString("00");

        timerString = hour + ":" + minute + ":" + second;
        timerText.text = timerString;

        if (Input.GetKeyDown("tab"))
        {
            transition.SetBool("PressKey", true);
        }
        if (Input.GetKeyUp("tab"))
        {
            transition.SetBool("PressKey", false);
        }
    }
}
