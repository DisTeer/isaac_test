using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AdvancedHealth : MonoBehaviour
{
    public Image[] lifeImgs;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    public Sprite halfHeart;

    public int maxHealth = 6;
    public int health;
    public EventManger eventManager;

    void Start()
    {
        eventManager.OnDamage.AddListener((changeHeart));
    }
    public void setHealth(int currentHealth)
    {
        health = currentHealth;
        changeHeart();
    }
    private void changeHeart()
    {
       switch (health)
        {
            case 0:
                lifeImgs[0].sprite = emptyHeart;
                lifeImgs[1].sprite = emptyHeart;
                lifeImgs[2].sprite = emptyHeart;
                break;
            case 1:
                lifeImgs[0].sprite = halfHeart;
                lifeImgs[1].sprite = emptyHeart;
                lifeImgs[2].sprite = emptyHeart;
                break;
            case 2:
                lifeImgs[0].sprite = fullHeart;
                lifeImgs[1].sprite = emptyHeart;
                lifeImgs[2].sprite = emptyHeart;
                break;
            case 3:
                lifeImgs[0].sprite = fullHeart;
                lifeImgs[1].sprite = halfHeart;
                lifeImgs[2].sprite = emptyHeart;
                break;
            case 4:
                lifeImgs[0].sprite = fullHeart;
                lifeImgs[1].sprite = fullHeart;
                lifeImgs[2].sprite = emptyHeart;
                break;
            case 5:
                lifeImgs[0].sprite = fullHeart;
                lifeImgs[1].sprite = fullHeart;
                lifeImgs[2].sprite = halfHeart;
                break;
            case 6:
                lifeImgs[0].sprite = fullHeart;
                lifeImgs[1].sprite = fullHeart;
                lifeImgs[2].sprite = fullHeart;
                break;
        }

    }
}
