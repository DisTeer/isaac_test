using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    public GameObject player;
    public bool picked = false;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        AudioSource audio = gameObject.GetComponent<AudioSource>();

        if (collision.CompareTag("Player") && gameObject.tag == "coin")
        {
            player.GetComponent<InventoryGestion>().PickCoin(1);
            picked = true;
        }
        else if (collision.CompareTag("Player") && gameObject.tag == "key")
        {
            player.GetComponent<InventoryGestion>().PickKey(1);
            picked = true;
        }
        else if (collision.CompareTag("Player") && gameObject.tag == "bomb")
        {
            player.GetComponent<InventoryGestion>().PickBomb(1);
            picked = true;
        }
        else if (collision.CompareTag("Player") && gameObject.tag == "Heart")
        {
            if (player.GetComponent<PlayerHealth>().health < 6)
            {
                player.GetComponent<PlayerHealth>().Heal(2);
                picked = true;
            }
        }
        if (picked)
        {
            audio.Play();
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Destroy(gameObject, 2f);
        }
    }
}